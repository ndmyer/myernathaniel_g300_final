﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraFollow : MonoBehaviour
{
    private CinemachineFreeLook fcam;
    GameObject currBot;

    //Transform target;

    private void Awake()
    {
        fcam = GetComponent<CinemachineFreeLook>();
    }

    private void Start()
    {
        currBot = GameObject.Find("BotBodyBase");
        //target = currBot.transform;

        fcam.m_LookAt = currBot.transform;
        fcam.m_Follow = currBot.transform;
    }
}

