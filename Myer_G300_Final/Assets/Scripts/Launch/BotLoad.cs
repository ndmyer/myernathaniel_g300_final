﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotLoad : MonoBehaviour
{
    private Rigidbody[] allChildren;
    private Rigidbody rb;

    GameObject childGO;

    Vector3 vecFix;
    Vector3 childPos;

    private int children;
    private int mass;

    // Start is called before the first frame update
    void Awake()
    {
        if (ES3.KeyExists("BotBodyBase"))
            ES3.Load<GameObject>("BotBodyBase");

        vecFix = new Vector3(1.5f, 9.5f, -111.5f);
        childGO = GameObject.Find("BotBodyBase");
        childGO.AddComponent<Rigidbody>();
        childGO.AddComponent<BotTag>();
        childGO.GetComponent<Rigidbody>().useGravity = false;
        childGO.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        //childGO.GetComponent<Collider>().isTrigger = true;
        childGO.transform.parent = this.transform;
        childGO.transform.position = vecFix;
        children = childGO.transform.childCount;
        mass = 1 + children;
        rb = childGO.GetComponent<Rigidbody>();
        rb.mass = mass;
    }
}
