﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlingshotScript : MonoBehaviour
{
    private Vector3 mOffset;
    private Vector3 slingStart;

    private float mZCoord;
    public float strength = 20f;

    //public GameObject botPrefab;
    public GameObject slingGo;
    private GameObject currBot;

    private bool ready = true;

    void Start()
    {
        slingStart = slingGo.transform.position;
        currBot = GameObject.Find("BotBodyBase");
    }

    // Creates an instance of botPrefab and parents it to slingGo
    void Update()
    {
        if (ready)
        {
            //currBot = Instantiate(botPrefab);
            //currBot.transform.parent = slingGo.transform;
            //currBot.transform.localPosition = Vector3.zero;
            ready = false;
        }
    }

    private void OnMouseDown()
    {
        //Store offset = GameObject world pos - MouseDragScript world pos
        mOffset = slingGo.transform.position - GetMouseWorldPos();
    }

    private void OnMouseUp()
    {
        Vector3 botPos = currBot.transform.position;

        slingGo.transform.position = slingStart;
        currBot.transform.parent = null;

        Rigidbody r = currBot.GetComponent<Rigidbody>();
        r.velocity = (slingStart - botPos) * strength;
        r.useGravity = true;

        //ready = true;

        currBot.GetComponent<Collider>().isTrigger = false;
    }

    private Vector3 GetMouseWorldPos()
    {
        mZCoord = Camera.main.WorldToScreenPoint(slingGo.transform.position).z;

        //Mouse position in pixel coordinates
        Vector3 mousePoint = Input.mousePosition;

        //z coordinate of selected game object
        mousePoint.z = mZCoord;

        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    //Called when an object is dragged by the mouse
    private void OnMouseDrag()
    {
        transform.position = GetMouseWorldPos() + mOffset;
    }
}
