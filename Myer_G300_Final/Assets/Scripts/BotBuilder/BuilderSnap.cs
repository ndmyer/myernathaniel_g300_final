﻿using UnityEngine;
using System.Collections;

public class BuilderSnap : MonoBehaviour
{

    public string partnerTag;
    public float closeVPDist = 0.05f;
    public float farVPDist = 1;
    public float moveSpeed = 40.0f;
    public float rotateSpeed = 8.0f;

    private Vector3 screenPoint;
    private Vector3 offset;
    //private bool isSnaped;
    Color color = new Color(1, 0, 0);

    float dist = Mathf.Infinity;
    Color normalColor;
    GameObject partnerGO;
    // Use this for initialization
    void Start()
    {
        normalColor = GetComponent<Renderer>().material.color;
        //partnerGO = GameObject.FindGameObjectWithTag(partnerTag);
    }

    /*
    private void Update()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("BotTag");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        partnerGO = closest;       
    } */

    void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(transform.position);
        offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        Cursor.visible = false;
    }
    void OnMouseDrag()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("BotTag");
        //GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                partnerGO = go;
                distance = curDistance;
            }
        }

        //partnerGO = closest;
        //transform.SetParent(null);
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        transform.position = curPosition;
        Vector3 partnerPos = Camera.main.WorldToViewportPoint(partnerGO.transform.position);
        Vector3 myPos = Camera.main.WorldToViewportPoint(transform.position);
        dist = Vector2.Distance(partnerPos, myPos);
        GetComponent<Renderer>().material.color = (dist < closeVPDist) ? color : normalColor;
    }
    void OnMouseUp()
    {
        Cursor.visible = true;
        if (dist < closeVPDist)
        {
            transform.SetParent(partnerGO.transform);
            //transform.SetParent(null);
            StartCoroutine(InstallPart());
            GetComponent<Renderer>().material.color = normalColor;
            //isSnaped = true;
            //gameObject.AddComponent<FixedJoint>();
            //GetComponent<FixedJoint>().connectedBody = partnerGO.GetComponentInParent<Rigidbody>();
        }
        if (dist > farVPDist)
        {
            transform.SetParent(null);
        }
    }
    IEnumerator InstallPart()
    {
        while (transform.localPosition != Vector3.zero || transform.localRotation != Quaternion.identity)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, Vector3.zero, Time.deltaTime * moveSpeed);
            transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.identity, rotateSpeed);
            yield return new WaitForEndOfFrame();
        }
    }

}
