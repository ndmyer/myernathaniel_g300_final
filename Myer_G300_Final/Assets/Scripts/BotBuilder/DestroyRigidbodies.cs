﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyRigidbodies : MonoBehaviour
{
    private Rigidbody[] allChildren;

    // If key is pressed makes an array of all object's childrens' rigidbodies and destroys them
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            allChildren = GetComponentsInChildren<Rigidbody>();
            foreach(Rigidbody rigidbody in allChildren) {
                DestroyImmediate(rigidbody);
            }
        }
    }
}
